import ProductItem from "./ProductItem";
import classes from "./Products.module.css";

const DUMMY_PRODUCTS = [
  {
    id: "p1",
    title: "Beautiful World, Where Are You",
    price: 25.76,
    description:
      "Beautiful World, Where Are You is a new novel by Sally Rooney, the bestselling author of Normal People and Conversations with Friends.",
  },
  {
    id: "p2",
    title: "Goodbye, Columbus: And Five Short Stories",
    price: 15.59,
    description:
      "Roth's award-winning first book instantly established its author's reputation as a writer of explosive wit, merciless insight, and a fierce compassion for even the most self-deluding of his characters.",
  },
  {
    id: "p3",
    title: "I Look Divine",
    price: 4.59,
    description:
      "Nicholas is beautiful, wealthy and hopelessly vain. With his older brother in tow, he jets from one glamorous scene to another: Rome, Madrid, Mexico. Wherever he goes, he seeks the admiration of other men, until one day - his beauty faded - he winds up dead, the victim of unknown circumstances. His brother is left to pick up and pieces and figure out how Nicholas came to his untimely end.",
  },
];

const Products = (props) => {
  return (
    <section className={classes.products}>
      <h2>Buy your favorite products</h2>
      <ul>
        {DUMMY_PRODUCTS.map((product) => (
          <ProductItem
            key={product.id}
            id={product.id}
            title={product.title}
            price={product.price}
            description={product.description}
          />
        ))}
      </ul>
    </section>
  );
};

export default Products;
