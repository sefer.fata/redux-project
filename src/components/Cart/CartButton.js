import { useDispatch, useSelector } from "react-redux";
import { userInterfaceActions } from "../../store/ui-slice";
import classes from "./CartButton.module.css";

const CartButton = (props) => {
  const totalItems = useSelector((state) => state.cart.totalItems);

  const dispatch = useDispatch();

  const cartToggleHandler = () => {
    dispatch(userInterfaceActions.toggle());
  };

  return (
    <button className={classes.button} onClick={cartToggleHandler}>
      <span>My Cart</span>
      <span className={classes.badge}>{totalItems}</span>
    </button>
  );
};

export default CartButton;
