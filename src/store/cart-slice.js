import { createSlice } from "@reduxjs/toolkit";

let cartItems = [];

const initialState = {
  cartItems,
  totalItems: 0,
  changed: false,
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    replaceCart(state, action) {
      state.totalItems = action.payload.totalItems;
      state.cartItems = action.payload.cartItems;
    },
    addItemToCart(state, action) {
      state.totalItems++;
      const newItem = action.payload;
      const existingItem = state.cartItems.find(
        (item) => item.id === newItem.id
      );

      state.changed = true;

      if (!existingItem) {
        state.cartItems.push({
          id: newItem.id,
          title: newItem.title,
          quantity: 1,
          total: newItem.price,
          price: newItem.price,
        });
      } else {
        existingItem.quantity++;
        existingItem.total = existingItem.price * existingItem.quantity;
      }
    },
    removeItemFromCart(state, action) {
      state.totalItems--;
      const id = action.payload;
      const existingItem = state.cartItems.find((item) => item.id === id);

      state.changed = true;
      if (existingItem.quantity > 1) {
        existingItem.quantity--;
        existingItem.total = existingItem.price * existingItem.quantity;
      } else {
        state.cartItems = state.cartItems.filter((item) => item.id !== id);
      }
    },
  },
});

export const cartActions = cartSlice.actions;

export default cartSlice.reducer;
